// leetcode.com
// Given an array of integers, return indices of the two numbers such that they add up to a specific target.
// You may assume that each input would have exactly one solution, and you may not use the same element twice.
// Example
// Given num = [2, 7, 11, 15], target = 9,
// Because num[0] + num[1] = 2 + 7 = 9,
// return [0, 1].

import java.util.Arrays;
import java.util.HashMap;

public class TwoSum {
    public int[] solution(int[] num, int target){
        int [] ans = new int[2];
        HashMap<Integer,Integer> data = new HashMap<>();
        for(int i = 0; i < num.length; i++){
            int key = target - num[i];
            if(data.containsKey(key)){
                ans[0] = data.get(key);
                ans[1] = i;
                return ans;
            }
            data.put(num[i],i);
        }
        return ans;
    }

    public static void main(String[] args) {
        TwoSum twoSum = new TwoSum();
        int [] num = {2,7,11,15};
        int [] result = twoSum.solution(num,9);
        for(int i : result){
            System.out.println(i);
        }

    }
}
