//  Given a Binary Search Tree (BST) with the root node root,
//  return the minimum difference between the values of any two different nodes in the tree.
//  Example :
//  Input: root = [4,2,6,1,3,null,null]
//  Output: 1
//  Explanation:
//  Note that root is a TreeNode object, not an array.
//
//  The given tree [4,2,6,1,3,null,null] is represented by the following diagram:
//
//          4
//        /   \
//       2      6
//      / \
//     1   3
//
// while the minimum difference in this tree is 1, it occurs between node 1 and node 2, also between node 3 and node 2.

import java.util.Stack;

public class MinimumDistanceBetweenBSTNodes {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode() {}
        TreeNode(int val) { this.val = val; }
        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public int solution(TreeNode root){
        Stack<TreeNode> stack = new Stack<>();
        int min = Integer.MAX_VALUE;
        stack.push(root);

        while (!stack.isEmpty()){
            TreeNode node = stack.pop();
            if(node.left != null){
                min = Math.min(min,Math.abs(node.val - node.left.val));
                stack.push(node.left);
            }
            if(node.right != null){
                min = Math.min(min,Math.abs(node.val - node.right.val));
                stack.push(node.left);
            }
        }
        return min;
    }

    public static void main(String[] args) {
        TreeNode node1 = new TreeNode(4);
        TreeNode node2 = new TreeNode(2,new TreeNode(8),new TreeNode(9));
        TreeNode node3 = new TreeNode(6);
        node1.left = node2;
        node1.right = node3;
        System.out.println(new MinimumDistanceBetweenBSTNodes().solution(node1));


    }
}

