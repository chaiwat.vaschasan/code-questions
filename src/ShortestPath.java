import java.util.*;

public class ShortestPath {

    public static void main(String[] args) {

        Graph graph = new Graph();
        ShortestPath shortestPath = new ShortestPath();

        graph.addVertex("A");
        graph.addVertex("B");
        graph.addVertex("C");

        graph.addEdge("A","B",2);
        graph.addEdge("A","C",2);
        graph.addEdge("B","C",5);

        System.out.println(shortestPath.solution(graph,"A","C"));


    }

    public List<String> solution(Graph graph,String start,String end){
        Vertex root = graph.getVertex(start);
        root.distance = 0;
        PriorityQueue<Vertex>  queue = new PriorityQueue<>();
        queue.add(root);

        while (!queue.isEmpty()){
            Vertex u = queue.poll();
            for(Edge e : u.edges){
                Vertex v = e.vertex;
                if((u.distance + e.weight) < v.distance ){
                    v.distance = (u.distance + e.weight);
                    v.previous = u;
                    queue.add(v);
                }
            }
        }

        Vertex path = graph.getVertex(end);
        List<String> pathString = new LinkedList<>();

        while (path.previous != null){
            pathString.add(path.label);
            path = path.previous;
        }
        pathString.add(root.label);
        Collections.reverse(pathString);
        return pathString;
    }

}

class Graph{

    private HashMap<String,Vertex> graph;

    public  Graph(){
        this.graph = new HashMap<>();
    }

    public void addVertex(String label){
        graph.put(label,new Vertex(label));
    }

    public Vertex getVertex(String label){
        return graph.get(label);
    }

    public void addEdge(String labelStart,String labelTo,int weight){
        Vertex vertexStart = getVertex(labelStart);
        Vertex vertexTo = getVertex(labelTo);
        Edge edge = new Edge(vertexTo,weight);
        vertexStart.edges.add(edge);
    }

}

class Vertex implements Comparable<Vertex>{

    int  distance;
    String label;
    Vertex previous;
    List<Edge> edges;

    public Vertex(String label){
        this.label = label;
        edges = new LinkedList<>();
        distance = Integer.MAX_VALUE;
    }

    @Override
    public int compareTo(Vertex o) {
        return Integer.compare(distance,o.distance);
    }
}

class Edge {
    Vertex vertex;
    int weight;

    public Edge(Vertex vertex,int weight){
        this.vertex = vertex;
        this.weight = weight;
    }
}
