// leetcode.com
// Merge two sorted linked lists and return it as a new list.
// The new list should be made by splicing together the nodes of the first two lists.
// Example:
// Input: 1->2->4, 1->3->4
// Output: 1->1->2->3->4->4

public class MergeTwoSortedLists {
     static class ListNode{
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    public ListNode solution(ListNode l1, ListNode l2){

         ListNode traveled = new ListNode(0);
         ListNode answer = traveled;

         while(l1 != null && l2 != null ){

            if(l1.val <= l2.val){
                traveled.val = l1.val;
                traveled.next = new ListNode(l2.val);
            }else{
                traveled.val = l2.val;
                traveled.next = new ListNode(l1.val);
            }

             l1 = l1.next;
             l2 = l2.next;
             if(l1 != null && l2 != null){
                 traveled.next.next = new ListNode();
             }

             traveled = traveled.next.next;
         }
         return answer;
    }

    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        l1.next = new ListNode(2);
        l1.next.next = new ListNode(4);

        ListNode l2 = new ListNode(1);
        l2.next = new ListNode(3);
        l2.next.next = new ListNode(5);

       ListNode ans = new MergeTwoSortedLists().solution(l1,l2);

       while (ans != null){
           System.out.println(ans.val);
           ans = ans.next;
       }
    }
}
